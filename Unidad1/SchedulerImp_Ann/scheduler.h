#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_STRING				100
#define MAX_TASK				100

#define UN_SEGUNDO				1
#define DOS_SEGUNDOS				2
#define TRES_SEGUNDOS				3
#define CUATRO_SEGUNDOS                         4
#define CINCO_SEGUNDOS 				5
#define SEIS_SEGUNDOS				6	
#define ACTIVO 					1
#define NO_ACTIVO				0

typedef struct TASK task;

// Definición de la estructura TASK
struct TASK{
	int id;
	int delay;
	char nombreTask[MAX_STRING];
	char proceso_realizar[MAX_STRING];
	int estado;
};

task *crear_Task(int _id, int _delay, char *_nombreTask, char *_proceso_realizar, int _estado);
void agregar_Task(task *_array, task _t, int _index);
void ejecutar_Tasks(task *_array);

#endif
