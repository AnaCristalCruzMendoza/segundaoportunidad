#include "scheduler.h"

int main(int argc, char const *argv[]){

	// @todo --> No repetir ID
	task *t1 = crear_Task(1, UN_SEGUNDO, "Tarea_1", "Reproducir Música", ACTIVO);
	task *t2 = crear_Task(2, DOS_SEGUNDOS, "Tarea_2", "Crear Carpeta Nueva", ACTIVO);
	task *t3 = crear_Task(3, TRES_SEGUNDOS, "Tarea_3", "Escribir Texto en Word", ACTIVO);
	task *t4 = crear_Task(4, TRES_SEGUNDOS, "Tarea_4", "Descargando Archivo", ACTIVO);
	task *t5 = crear_Task(4, TRES_SEGUNDOS, "Tarea_5", "Abriendo Google Chrome", ACTIVO);
	task *t6 = crear_Task(4, TRES_SEGUNDOS, "Tarea_6", "Mensajeria ", ACTIVO);	

	task *array = (task *)malloc(sizeof(task)*MAX_TASK);

	agregar_Task(array, *t1, 0);
	agregar_Task(array, *t2, 1);
	agregar_Task(array, *t3, 2);
	agregar_Task(array, *t4, 3);	
	agregar_Task(array, *t5, 4);
	agregar_Task(array, *t6, 5);
	
	ejecutar_Tasks(array);

	free(t1);
	free(t2);
	free(t3);
	free(t4);
	free(t5);
	free(t6);
	return 0;
}
