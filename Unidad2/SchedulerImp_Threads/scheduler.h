#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_STRING				100
#define MAX_TASK				100

#define UN_SEGUNDO				1
#define DOS_SEGUNDOS			2
#define TRES_SEGUNDOS			3
#define CUATRO_SEGUNDOS			4
	
#define ACTIVO 					1
#define NO_ACTIVO				0

int cont_tasks;

typedef struct TASK task;
typedef struct ARRAY_TASK array_task;

typedef void *(*TASK_ACCION)(void *); /*Puntero a funcion que retorna un puntero de tipo void y recibe un puntero de tipo void.*/

// Definición de la estructura TASK
struct TASK{
	int id;
	int delay;
	char nombreTask[MAX_STRING];
	char proceso_realizar[MAX_STRING];
	TASK_ACCION accion;
	int estado;
};

struct ARRAY_TASK{
	task tarea;
	int empty;
};

task *crear_Task(int _id, int _delay, char *_nombreTask, char *_proceso_realizar, int _estado, TASK_ACCION _accion);
void agregar_Task(array_task *_array, task _t);
void ejecutar_Tasks(array_task *_array);

array_task *crear_ArrayTasks(void);

#endif